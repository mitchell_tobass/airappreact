import React, { Component } from 'react';

export default class App extends Component {
    static displayName = App.name;

    constructor(props) {
        super(props);
        this.state = { interval: 30, arrivalData: [], loading: true };
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    static renderArrivals(arrivalData) {
        if (arrivalData.error) {
            return (
                <div>
                    <div class="error" >Произошла ошибка!</div>
                    <p>{arrivalData.error}</p>
                </div>
            );
        }

        return (
            <div>
                Наименьшее количество прибытий в интервале: {arrivalData.minArrival} — {arrivalData.minDueArrival}
                <p>Наибольший интервал без прилёта рейсов: {arrivalData.noArrival} — {arrivalData.noDueArrival}</p>
            </div>
        );
    }

    handleChange(event) {
        this.setState({ interval: event.target.value })
    }

    handleClick(event) {
        this.fetchArrivalData()
        event.preventDefault();
    }

    render() {
        let contents = this.state.loading ? "" : App.renderArrivals(this.state.arrivalData);

        return (
            <div class="myForm">
                <h1 id="tabelLabel" >Время прибытия в аэропорт Шереметьево</h1>
                {contents}

                <form onSubmit={this.handleClick}>
                    <label>
                        Временной интервал:
                        <div>
                        <input type="number" value={this.state.interval} onChangeCapture={this.handleChange} step="5" min="30" max="240" />
                            <div>мин</div></div>
                    </label>
                    <p></p>
                    <button type="submit">Получить</button>
                </form>

            </div>
        );
    }

    async fetchArrivalData() {
        const response = await fetch(`arriving?interval=${encodeURIComponent(this.state.interval)}`);
        const data = await response.json();
        this.setState({ arrivalData: data, loading: false });
    }
}
