namespace webapi;

static public class Utility
{
    /// <summary>
    /// Метод преобразования API ключа к исходному виду
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    static public string IncrementChars(string input)
    {
        char[] charArray = input.ToCharArray();

        for (int i = 0; i < charArray.Length; i++)
        {
            charArray[i]++;
        }

        return new string(charArray);
    }

    /// <summary>
    /// Преобразуем 5-min интервалы в DateTime
    /// </summary>
    /// <param name="intervals">Количество интервалов</param>
    /// <returns></returns>
    static public DateTime IntervalToDate(int intervals) =>
        DateOnly.FromDateTime(DateTime.Now)
        .ToDateTime(TimeOnly.MinValue)
        .Add(TimeSpan.FromMinutes(intervals * 5));
}
