using Microsoft.AspNetCore.Mvc;
using System.Globalization;
using System.Text.Json;

namespace webapi.Controllers;

[ApiController]
[Route("[controller]")]
public class ArrivingController : ControllerBase
{
    private readonly IConfiguration _configuration;
    private readonly SemaphoreSlim semaphore = new SemaphoreSlim(1, 1);

    public ArrivingController(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    [HttpGet]
    [Route("GetArriving")]
    public async Task<JsonResult> Get(int interval)
    {
        if (interval < 30 || interval > 240 || interval % 5 != 0)
        {
            return new JsonResult(new { error = "Incorrect interval value" });
        }

        string json1, json2;
        string apiKey = Utility.IncrementChars(_configuration["ApiYandexRasp"]);

        await semaphore.WaitAsync();

        try
        {
            if (System.IO.File.Exists("rasp1.txt") && System.IO.File.Exists("rasp2.txt"))
            {
#if DEBUG
                Console.WriteLine($"Обнаружены файлы расписания от {DateOnly.FromDateTime(System.IO.File.GetCreationTime("rasp1.txt"))}, данные будут считаны с него");
#endif
                json1 = System.IO.File.ReadAllText("rasp1.txt");
                json2 = System.IO.File.ReadAllText("rasp2.txt");
            }
            else
            {
                Console.WriteLine("Обновляем данные расписания");
                var Uri = $"https://api.rasp.yandex.net/v3.0/schedule/?apikey={apiKey}&station=s9600213&transport_types=plane&event=arrival&limit=1000";
                try
                {
                    using (var client = new HttpClient())
                    {
                        json1 = await client.GetStringAsync(Uri + "&date=" + DateTime.UtcNow.ToString("o", CultureInfo.InvariantCulture));
                        json2 = await client.GetStringAsync(Uri + "&date=" + DateTime.UtcNow.AddDays(1).ToString("o", CultureInfo.InvariantCulture));
                        System.IO.File.WriteAllText("rasp1.txt", json1);
                        System.IO.File.WriteAllText("rasp2.txt", json2);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    Console.WriteLine("Ошибка при обновлении данных!");
                    return new JsonResult(new { error = "Internal server error" });
                }
            }
        }
        finally
        {
            semaphore.Release();
        }

        var data1 = JsonSerializer.Deserialize<Root>(json1);
        var data2 = JsonSerializer.Deserialize<Root>(json2);

        if (data1 is null || data2 is null)
        {
            return new JsonResult(new { error = "Unable to deserealize data" });
        }

        const int intervalsPerDay = 60 / 5 * 24; // 60m / 5m interval * 24h
        var ranges = new int[intervalsPerDay * 2];

        foreach (var item in data1.schedule)
        {
            int rng_idx = (int)TimeOnly.FromDateTime(DateTime.Parse(item.arrival)).ToTimeSpan().TotalMinutes / 5;
            ranges[rng_idx]++;
        }
        foreach (var item in data2.schedule)
        {
            int rng_idx = (int)TimeOnly.FromDateTime(DateTime.Parse(item.arrival)).ToTimeSpan().TotalMinutes / 5;
            ranges[rng_idx + intervalsPerDay]++;
        }

        int minEmpty = 0, maxCountEmpty = -1, startEmpty = 0, countEmpty = 0;
        int startFly = 0, maxCountFly = int.MaxValue, countFly = 0, intervalFly = interval / 5;

        for (int i = 0; i < ranges.Length; i++)
        {
            if (ranges[i] == 0)
            {
                if (countEmpty == 0)
                    startEmpty = i;

                if (maxCountEmpty < countEmpty)
                {
                    minEmpty = startEmpty;
                    maxCountEmpty = countEmpty;
                }

                countEmpty++;

#if DEBUG
                //Console.WriteLine("В это время рейсов нет: " + TimeSpan.FromMinutes(i * 5));
#endif
            }
            else
            {
                countEmpty = 0;
            }

            countFly += ranges[i];
            if (i > intervalFly)
            {
                countFly -= ranges[i - intervalFly];
                if (countFly < maxCountFly &&         // Наименьшее количество перелётов в интервале
                    countFly > 0 &&                   // В нём должен быть хоть 1 перелёт
                    ranges[i - intervalFly + 1] > 0)  // Начало интервала это существующий рейс
                {
                    maxCountFly = countFly;
                    startFly = i - intervalFly + 1;
                }
            }
        }

        return new JsonResult(new
        {
            minArrival = Utility.IntervalToDate(startFly).ToString(),
            minDueArrival = Utility.IntervalToDate(startFly + intervalFly).ToString(),
            noArrival = Utility.IntervalToDate(minEmpty).ToString(),
            noDueArrival = Utility.IntervalToDate(minEmpty + maxCountEmpty).ToString(),
        });
    }
}
